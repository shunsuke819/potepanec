require 'rails_helper'

RSpec.describe 'Products', type: :feature do
  before do
    visit potepan_product_path product.id
  end

  include_context "product setup"

  scenario 'showページへ遷移する' do
    expect(page).to have_title 'Ruby on rails - BIGBAG Store'
    within('div.media-body') do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    expect(page).to have_link 'Home'
  end

  scenario 'トップページへ移動する' do
    click_link 'Home', match: :first
    expect(current_path).to eq potepan_path
  end
end
