require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "showページにアクセスした時" do
    before do
      get potepan_product_path product.id
    end

    include_context "product setup"

    it "リクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "タイトルが正しく表示すること" do
      expect(response.body).to include 'Ruby on rails - BIGBAG Store'
    end

    it "商品名が表示されていること" do
      within('div.media-body') do
        expect(response.body).to include product.name
      end
    end

    it "金額が表示されていること" do
      within('div.media-body') do
        expect(response.body).to include product.display_price
      end
    end

    it "商品説明が表示されていること" do
      within('div.media-body') do
        expect(response.body).to include product.description
      end
    end
  end

  describe "friendly_idでshowページにアクセスした時" do
    include_context "product setup"

    it "リクエストが成功すること" do
      get potepan_product_path product.friendly_id
      expect(response).to have_http_status(200)
    end
  end
end
